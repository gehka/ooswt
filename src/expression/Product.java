package expression;

public class Product implements Summand{

    private Expression firstFactor;
    private Expression secondFactor;

    public Product(Expression firstFactor, Expression secondFactor) {
        this.firstFactor = firstFactor;
        this.secondFactor = secondFactor;
    }

    @Override
    public Integer evaluate() {
        return (this.firstFactor.evaluate() * this.secondFactor.evaluate());
    }
}
