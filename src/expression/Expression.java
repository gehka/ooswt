package expression;

public interface Expression {

    Integer evaluate();
}
