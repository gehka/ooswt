package expression;

public class NaturalNumber implements Factor {
    private Integer value;

    public NaturalNumber(Integer value) {
        this.value = value;
    }

    @Override
    public Integer evaluate() {
        return this.value;
    }
}
