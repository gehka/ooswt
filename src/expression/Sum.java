package expression;

public class Sum implements Expression {

    private Expression firstSummand;
    private Expression secondSummand;

    public Sum(Expression firstSummand, Expression secondSummand) {
        this.firstSummand = firstSummand;
        this.secondSummand = secondSummand;

    }

    @Override
    public Integer evaluate() {
        return this.firstSummand.evaluate() + this.secondSummand.evaluate();
    }

    public String toString(){
        return this.firstSummand + ", " + this.secondSummand;
    }
}
