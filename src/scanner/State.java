package scanner;

/**
 * Scanner-State according to State Pattern
 */
public abstract class State {
    /**
     * May add symbol to current Scanner result depending on <c>,
     * May erase <c> in input string
     * May change state
     */
    private Scanner scanner;

    public State(Scanner scanner) {
        this.scanner = scanner;
    }

    public Scanner getScanner() {
        return this.scanner;
    }

    public abstract void scan(Character c);

    public void exit() {
        
    };

    public void endOfProcessing() {
        this.scanner.setState(new SelectionState(scanner));
    }


}

// Alternative als Abstrakte Klasse: 
// public abstract class StateX{
//    private Scanner myScanner;
//	public StateX(Scanner myScanner) {
//		this.myScanner = myScanner;
//	}
// Allgemeines Ende der Verarbeitung - vorhanden in allen implementierenden Klassen

// etc -> wird in der n�chsten Woche noch pr�zisiert
