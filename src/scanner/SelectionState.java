package scanner;

import static basic.TextConstants.*;

/**
 * Selection of next state depending on next symbol 
 */
public class SelectionState extends State {

	public SelectionState(Scanner scanner) {
		super(scanner);
	}

	@Override
	public void scan(Character c) {
		if(Character.isDigit(c)) 
			this.getScanner().setState(new DigitState(getScanner()));
		else if(Character.isWhitespace(c))
			this.getScanner().setState(new WhitespaceState(getScanner()));
		else if(OperatorState.handles(c))
			this.getScanner().setState(new OperatorState(getScanner()));
		else if(BracketState.handles(c))
			this.getScanner().setState(new BracketState(getScanner()));
		else
			this.getScanner().setState(new ErrorState(getScanner()));
	}


}
