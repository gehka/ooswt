package scanner;

import java.util.ArrayList;
import java.util.List;

import basic.TextConstants;
import symbols.Symbol;

/**
 * The main entry point for lexical analysis of string expressions
 */
public class Scanner {
    private State state;                // State Pattern
    private String currentExpression;   // Input (shrinks during scanning)
    private List<Symbol> currentResult;    // Output (grows during scanning)

    public Scanner() {
        this.currentExpression = TextConstants.emptyString;
        this.currentResult = new ArrayList<Symbol>();
        this.state = new SelectionState(this);
    }

    /**
     * Ingredient of State Pattern
     */
    void setState(State newState) {
        this.state = newState;
    }

    /**
     * EFFECTS: Transformation of input string expr into sequence (list) of symbols
     */
    public List<Symbol> toSymbolSequence(String expr) {
        this.currentExpression = expr;                // Enable manipulation of expr from external objects
        while (this.currentExpression.length() > 0) {  // Some character still to be scanned
            this.state.scan(this.currentExpression.charAt(0));
        }
        this.state.exit();
        return currentResult;
    }

    /**
     * Reduces currentExpression by deleting first character,
     * only if currentExpression is not empty
     */
    //TODO Testen
    public void skip() {
        if (this.currentExpression.length() > 0)
            this.currentExpression = this.currentExpression.substring(1);
    }

    /**
     * Adds a symbol to currentResult
     */
    //TODO Testen
    public void addSymbol(Symbol symbol) {
        this.currentResult.add(symbol);
    }

}







