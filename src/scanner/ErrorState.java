package scanner;

import symbols.ErrorToken;
import static basic.TextConstants.*;
import static basic.TextConstants.starSymbol;

public class ErrorState extends State {

    private StringBuilder errorChars;

    public ErrorState(Scanner scanner) {
        super(scanner);
        this.errorChars = new StringBuilder();
    }


    @Override
    public void scan(Character c) {
        if(this.isErrorChar(c)) {
            errorChars.append(c);
            getScanner().skip();
        } else {
            this.exit();
            this.endOfProcessing();
        }
    }

    @Override
    public void exit() {
        getScanner().addSymbol(new ErrorToken(errorChars.toString()));
    }

    /**
     * Returns true if the Character c is not a valid Character. Valid Characters are
     * ' ', '(', ')', '+', '*', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
     * @param c
     * @return
     */
    public boolean isErrorChar(Character c) {
        if (Character.isDigit(c))
            return false;
        else if (Character.isWhitespace(c))
            return false;
        else if (c.equals(leftBracketSymbol) || c.equals(rightBracketSymbol))
            return false;
        else if (c.equals(plusSymbol) || c.equals(starSymbol))
            return false;
        else
            return true;
    }
}
