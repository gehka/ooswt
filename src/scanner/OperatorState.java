package scanner;

import symbols.StarSymbol;
import symbols.PlusSymbol;
import symbols.Symbol;

import java.util.HashMap;
import java.util.Map;

import static basic.TextConstants.*;

public class OperatorState extends State {

    private static final Map<Character, Symbol> operatorSymbols = new HashMap<Character, Symbol>(){{put(plusSymbol, new PlusSymbol());
        put(starSymbol, new StarSymbol());
    }};

    public OperatorState(Scanner scanner) {
        super(scanner);
    }

    public static boolean handles(Character c) {
        return operatorSymbols.containsKey(c);
    }

    @Override
    public void scan(Character c) {
        getScanner().addSymbol(operatorSymbols.get(c));
        getScanner().skip();
        this.endOfProcessing();
    }

}
