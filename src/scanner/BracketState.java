package scanner;

import symbols.LeftBracketSymbol;
import symbols.RightBracketSymbol;
import symbols.Symbol;

import java.util.HashMap;
import java.util.Map;

import static basic.TextConstants.*;

public class BracketState extends State {

    public BracketState(Scanner scanner) {
        super(scanner);
    }

    private static final Map<Character, Symbol> bracketSymbols = new HashMap<Character, Symbol>(){{put(leftBracketSymbol, new LeftBracketSymbol());
        put(rightBracketSymbol, new RightBracketSymbol());
    }};

    public static boolean handles(Character c) {
        return bracketSymbols.containsKey(c);
    }

    @Override
    public void scan(Character c) {
        getScanner().addSymbol(bracketSymbols.get(c));
        getScanner().skip();
        this.endOfProcessing();
    }

}
