package scanner;
/**
 * Responsible for processing scanner input's whitespace 
 */
public class WhitespaceState extends State {

	public WhitespaceState(Scanner scanner) {
		super(scanner);
	}

	@Override
	public void scan(Character c) {
		getScanner().skip();
		this.endOfProcessing();
	}

}
