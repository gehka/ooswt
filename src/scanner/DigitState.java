package scanner;

import symbols.NumberSymbol;

/**
 * Digit Processing
 */
public class DigitState extends State {
    private Integer collectedDigits;

    public DigitState(Scanner scanner) {
        super(scanner);
        this.collectedDigits = 0;
    }

    @Override
    public void scan(Character c) {
        if (Character.isDigit(c)) {
            this.collectedDigits = 10 * this.collectedDigits + Integer.parseInt(c.toString());
            getScanner().skip();
        } else {
            this.exit();
            this.endOfProcessing();
        }
    }

    @Override
    public void exit() {
        getScanner().addSymbol(new NumberSymbol(collectedDigits));
    }

}
