package basic;

/**
 * Constants
 */
public abstract class TextConstants {
	public static final String emptyString = "";
	public static final Character plusSymbol = '+';
	public static final Character starSymbol = '*';
	public static final Character leftBracketSymbol = '(';
	public static final Character rightBracketSymbol = ')';
}
