package parser;

import expression.*;
import symbols.*;
import visitors.SymbolVisitor;

import java.util.List;

public class SummandParser implements SymbolVisitor {

    private List<Symbol> symbolList;
    private Expression expression;
    private Expression finalExpression;

    public SummandParser(List<Symbol> symbolList) {
        this.symbolList = symbolList;
    }

    public Expression toExpression() {
        this.expression = new FactorParser(this.symbolList).toExpression();
        this.symbolList.get(0).accept(this);
        return finalExpression;
    }

    @Override
    public void handleNumberSymbol(NumberSymbol numberNumber) {
    }

    @Override
    public void handlePlusSymbol(PlusSymbol plusSymbol) {
        this.finalExpression = this.expression;

    }

    @Override
    public void handleStarSymbol(StarSymbol starSymbol) {
        this.symbolList.remove(0);
        this.finalExpression = new Product(this.expression, new SummandParser(symbolList).toExpression());
    }

    @Override
    public void handleLeftBracketSymbol(LeftBracketSymbol leftBracketSymbol) {

    }

    @Override
    public void handleRightBracketSymbol(RightBracketSymbol rightBracketSymbol) {
        this.finalExpression = this.expression;
    }

    @Override
    public void handleErrorToken(ErrorToken errorToken) {

    }

    @Override
    public void handleEndSymbol(EndSymbol endSymbol) {
        this.finalExpression = this.expression;

    }
}
