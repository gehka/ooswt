package parser;

import expression.BracketExpression;
import expression.Expression;
import expression.NaturalNumber;
import symbols.*;
import visitors.SymbolVisitor;

import java.util.List;


public class FactorParser implements SymbolVisitor {

    private List<Symbol> symbolList;
    private Expression expression;
    private Expression finalExpression;

    public FactorParser(List<Symbol> symbolList) {
        this.symbolList = symbolList;
    }

    public Expression toExpression() {
        symbolList.get(0).accept(this);
        return finalExpression;
    }

    @Override
    public void handleNumberSymbol(NumberSymbol numberSymbol) {
        this.finalExpression = new NaturalNumber(numberSymbol.getValue());
        this.symbolList.remove(0);
    }

    @Override
    public void handlePlusSymbol(PlusSymbol plusSymbol) {

    }

    @Override
    public void handleStarSymbol(StarSymbol starSymbol) {

    }

    @Override
    public void handleLeftBracketSymbol(LeftBracketSymbol leftBracketSymbol) {
        this.symbolList.remove(0);
        this.finalExpression = new BracketExpression(new ExpressionParser(symbolList).toExpression());

    }

    @Override
    public void handleRightBracketSymbol(RightBracketSymbol rightBracketSymbol) {
    }

    @Override
    public void handleErrorToken(ErrorToken errorToken) {

    }

    @Override
    public void handleEndSymbol(EndSymbol endSymbol) {

    }
}
