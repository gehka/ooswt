package parser;

import expression.Expression;
import symbols.EndSymbol;
import symbols.Symbol;

import java.util.List;

public class ExpressionParserProxy {

    public Expression toExpression(List<Symbol> symbolList) {
        symbolList.add(new EndSymbol());
        return new ExpressionParser(symbolList).toExpression();
    }
}
