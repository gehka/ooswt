package parser;

import expression.Expression;
import expression.Sum;
import symbols.*;
import visitors.SymbolVisitor;

import java.util.List;

class ExpressionParser implements SymbolVisitor {

    private List<Symbol> symbolList;
    private Expression expression;
    private Expression finalExpression;


    public ExpressionParser(List<Symbol> symbolList) {
        this.symbolList = symbolList;
    }

    public Expression toExpression() {
        this.expression = new SummandParser(this.symbolList).toExpression();
        this.symbolList.get(0).accept(this);
        return finalExpression;
    }

    @Override
    public void handleNumberSymbol(NumberSymbol numberNumber) {

    }

    @Override
    public void handlePlusSymbol(PlusSymbol plusSymbol) {
        this.symbolList.remove(0);
        this.finalExpression = new Sum(this.expression, new ExpressionParser(this.symbolList).toExpression());
    }

    @Override
    public void handleStarSymbol(StarSymbol starSymbol) {

        System.out.println("test");
    }

    @Override
    public void handleLeftBracketSymbol(LeftBracketSymbol leftBracketSymbol) {

    }

    @Override
    public void handleRightBracketSymbol(RightBracketSymbol rightBracketSymbol) {
        this.finalExpression = this.expression;
        this.symbolList.remove(0);
        this.symbolList.get(0).accept(this);
    }

    @Override
    public void handleErrorToken(ErrorToken errorToken) {

    }

    @Override
    public void handleEndSymbol(EndSymbol endSymbol) {
        this.finalExpression = this.expression;
    }
}
