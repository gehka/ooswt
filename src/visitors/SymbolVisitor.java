package visitors;


import symbols.*;

public interface SymbolVisitor {

    void handleNumberSymbol(NumberSymbol numberNumber);
    void handlePlusSymbol(PlusSymbol plusSymbol);
    void handleStarSymbol(StarSymbol starSymbol);
    void handleLeftBracketSymbol(LeftBracketSymbol leftBracketSymbol);
    void handleRightBracketSymbol(RightBracketSymbol rightBracketSymbol);
    void handleErrorToken(ErrorToken errorToken);
    void handleEndSymbol(EndSymbol endSymbol);
}
