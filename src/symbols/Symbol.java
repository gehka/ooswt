package symbols;

import visitors.SymbolVisitor;

/**
 *  Extracted Symbols (e.g. Numbers, Operators, ...) from Scanner's input
 */
public interface Symbol {

    void accept(SymbolVisitor symbolVisitor);
}
