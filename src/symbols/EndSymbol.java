package symbols;

import visitors.SymbolVisitor;

public class EndSymbol implements Symbol {
    @Override
    public void accept(SymbolVisitor symbolVisitor) {
        symbolVisitor.handleEndSymbol(this);
    }
}
