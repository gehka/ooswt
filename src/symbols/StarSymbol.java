package symbols;

import visitors.SymbolVisitor;

public class StarSymbol implements OperatorSymbol {

    //TODO Testen
    public boolean equals(Object obj) {
        if(obj==null) return false;
        return obj instanceof StarSymbol;
    }

    @Override
    public void accept(SymbolVisitor symbolVisitor) {
        symbolVisitor.handleStarSymbol(this);
    }

    //TODO toString()
}
