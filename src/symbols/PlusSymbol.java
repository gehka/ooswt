package symbols;

import visitors.SymbolVisitor;

public class PlusSymbol implements OperatorSymbol {
    public boolean equals(Object obj) {
        if(obj==null) return false;
        return obj instanceof PlusSymbol;
    }

    @Override
    public void accept(SymbolVisitor symbolVisitor) {
        symbolVisitor.handlePlusSymbol(this);
    }
    //TODO Testen

    //TODO toString()
}
