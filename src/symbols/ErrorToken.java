package symbols;

import visitors.SymbolVisitor;

public class ErrorToken implements Symbol {

    private String value;

    public ErrorToken(String value) {
        this.value = value;
    }

    //TODO Testen
    public boolean equals(Object obj) {
        if(obj==null) return false;
        if(!(obj instanceof ErrorToken)) return false;
        ErrorToken other = (ErrorToken) obj;
        return this.value.equals(other.value);
    }

    @Override
    public void accept(SymbolVisitor symbolVisitor) {
        symbolVisitor.handleErrorToken(this);
    }

    //TODO toString()
}
