package symbols;

import visitors.SymbolVisitor;

/**
 * Natural Number
 */
public class NumberSymbol implements Symbol {
    private Integer value;

    public NumberSymbol(Integer value) {
        this.value = value;
    }

    //TODO Testen
    public boolean equals(Object obj) { // Object entspricht Anything, return beendet die Methode
        if (obj == null) return false;     // Wenn obj null, dann ungleich
        if (!(obj instanceof NumberSymbol)) return false; // Wenn falscher Typ, dann ungleich
        NumberSymbol other = (NumberSymbol) obj; // Typanpassung: obj als NumberSymbol typisieren
        return this.value.equals(other.value); // Gleichheit, wenn values gleich sind
    }

    @Override
    public void accept(SymbolVisitor symbolVisitor) {
        symbolVisitor.handleNumberSymbol(this);
    }

    public Integer getValue() {
        return this.value;
    }

    //TODO toString()
}

