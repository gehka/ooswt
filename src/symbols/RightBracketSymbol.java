package symbols;

import visitors.SymbolVisitor;

public class RightBracketSymbol implements BracketSymbol {

    //TODO Testen
    public boolean equals(Object obj) {
        if(obj==null) return false;
        return obj instanceof RightBracketSymbol;
    }

    @Override
    public void accept(SymbolVisitor symbolVisitor) {
        symbolVisitor.handleRightBracketSymbol(this);
    }

    //TODO toString()
}
