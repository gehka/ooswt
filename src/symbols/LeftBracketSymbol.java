package symbols;

import visitors.SymbolVisitor;

public class LeftBracketSymbol implements BracketSymbol{

    public boolean equals(Object obj) {
        if(obj==null) return false;
        return obj instanceof LeftBracketSymbol;
    }

    @Override
    public void accept(SymbolVisitor symbolVisitor) {
        symbolVisitor.handleLeftBracketSymbol(this);
    }

    //TODO toString()
}
