import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import parser.ExpressionParserProxy;
import scanner.Scanner;

class ExpressionTests {

    Scanner scanner = new Scanner();
    ExpressionParserProxy parser = new ExpressionParserProxy();

    @Test
    void testSimpleExpr1() {
        String expr = "5";
        assertEquals(5, parser.toExpression(scanner.toSymbolSequence(expr)).evaluate());
    }

    @Test
    void testSimpleExpr2() {
        String expr = "1+2";
        assertEquals(3, parser.toExpression(scanner.toSymbolSequence(expr)).evaluate());
    }

    @Test
    void testSimpleExpr3() {
        String expr = "(5)";
        assertEquals(5, parser.toExpression(scanner.toSymbolSequence(expr)).evaluate());
    }

    @Test
    void testSimpleExpr4() {
        String expr = "2*3+4";
        assertEquals(10, parser.toExpression(scanner.toSymbolSequence(expr)).evaluate());
    }

    @Test
    void testSimpleExpr5() {
        String expr = "4+3*2";
        assertEquals(10, parser.toExpression(scanner.toSymbolSequence(expr)).evaluate());
    }

    @Test
    void testSimpleExpr6() {
        String expr = "2+(3*4)";
        assertEquals(14, parser.toExpression(scanner.toSymbolSequence(expr)).evaluate());
    }


    @Test
    void testSimpleExpr7() {
        String expr = "(2+3)*4";
        assertEquals(20, parser.toExpression(scanner.toSymbolSequence(expr)).evaluate());
    }

    @Test
    void testSimpleExpr8() {
        String expr = "(2+3*4)";
        assertEquals(14, parser.toExpression(scanner.toSymbolSequence(expr)).evaluate());
    }

    @Test
    void testSimpleExpr9() {
        String expr = "2+(3*4)";
        assertEquals(14, parser.toExpression(scanner.toSymbolSequence(expr)).evaluate());
    }

    @Test
    void testSimpleExpr10() {
        String expr = "2*(3+4)";
        assertEquals(14, parser.toExpression(scanner.toSymbolSequence(expr)).evaluate());
    }

    @Test
    void testComplexExpr1() {
        String expr = "1+(2*(3+4))";
        assertEquals(15, parser.toExpression(scanner.toSymbolSequence(expr)).evaluate());
    }

    @Test
    void testComplexExpr2() {
        String expr = "(1+2)*(2*(3+4))";
        assertEquals(42, parser.toExpression(scanner.toSymbolSequence(expr)).evaluate());
    }

}
