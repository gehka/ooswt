import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import scanner.Scanner;
import symbols.*;

class ScannerTests {

	@Test
	void testEmptySequence() {
		Scanner scanner = new Scanner();
		List<Symbol> result = new ArrayList<Symbol>();
		assertEquals(result, scanner.toSymbolSequence(""));
	}
	@Test
	void testDigitSequence() {
		Scanner scanner = new Scanner();
		List<Symbol> result = new ArrayList<Symbol>();
		result.add(new NumberSymbol(123));
		assertEquals(result, scanner.toSymbolSequence("123"));
	}
	@Test
	void testWhitespace() {
		Scanner scanner = new Scanner();
		List<Symbol> result = new ArrayList<Symbol>();
		result.add(new NumberSymbol(123));
		assertEquals(result, scanner.toSymbolSequence("123 "));
	}

	@Test
	void testWhitespaceBetweenDigits() {
		Scanner scanner = new Scanner();
		List<Symbol> result = new ArrayList<Symbol>();
		result.add(new NumberSymbol(1));
		result.add(new NumberSymbol(2));
		result.add(new NumberSymbol(3));
		assertEquals(result, scanner.toSymbolSequence("1 2 3"));
	}
	@Test
	void testPlusSymbol() {
		Scanner scanner = new Scanner();
		List<Symbol> result = new ArrayList<Symbol>();
		result.add(new PlusSymbol());
		assertEquals(result, scanner.toSymbolSequence("+"));
	}
	@Test
	void testOperatorSymbol() {
		Scanner scanner = new Scanner();
		List<Symbol> result = new ArrayList<Symbol>();
		result.add(new PlusSymbol());
		result.add(new StarSymbol());
		assertEquals(result, scanner.toSymbolSequence("+ *"));
	}
	@Test
	void testStarSymbol() {
		Scanner scanner = new Scanner();
		List<Symbol> result = new ArrayList<Symbol>();
		result.add(new StarSymbol());
		assertEquals(result, scanner.toSymbolSequence("*"));
	}
	@Test
	void testBracketSymbol() {
		Scanner scanner = new Scanner();
		List<Symbol> result = new ArrayList<Symbol>();
		result.add(new LeftBracketSymbol());
		result.add(new RightBracketSymbol());
		assertEquals(result, scanner.toSymbolSequence("()"));
	}
	@Test
	void testErrorToken() {
		Scanner scanner = new Scanner();
		List<Symbol> result = new ArrayList<Symbol>();
		result.add(new ErrorToken("abc"));
		assertEquals(result, scanner.toSymbolSequence("abc"));
	}
	@Test
	void testComplexSequence() {
		Scanner scanner = new Scanner();
		List<Symbol> result = new ArrayList<Symbol>();
		result.add(new ErrorToken("abc"));
		result.add(new NumberSymbol(123));
		result.add(new ErrorToken("d"));
		result.add(new StarSymbol());
		result.add(new LeftBracketSymbol());
		result.add(new NumberSymbol(4));
		result.add(new PlusSymbol());
		result.add(new NumberSymbol(5));
		result.add(new RightBracketSymbol());
		assertEquals(result, scanner.toSymbolSequence("abc   123d *    (	4+5) "));
	}
}
